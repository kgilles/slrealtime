const express = require('express');
const request = require('request');

const app = express();
app.set('view engine', 'pug');
app.set('views', __dirname + '/views');

const apiKey = '0f78de15ee2f4c849c4d477ca10b6dc7';
const siteId = 3514;
const timeWindow = 30;
const url = `http://api.sl.se/api2/realtimedeparturesV4.json?key=${apiKey}&siteid=${siteId}&timewindow=${timeWindow}&metro=false&train=false&tram=false&ship=false`;

function minutesUntilDeparture(departureTime) {
  const departureMinutes = departureTime.split(':')[1];
  const departureSeconds = departureTime.split(':')[2];

  const date = new Date();
  const currentMinutes = date.getMinutes();
  const currentSeconds = date.getSeconds();

  let minutesToGo = departureMinutes - currentMinutes;
  let secondsToGo = departureSeconds - currentSeconds;

  if (minutesToGo < 0) minutesToGo = 60 - (minutesToGo * -1);
  if (secondsToGo < 0) secondsToGo = 60 - (secondsToGo * -1);

  return `${minutesToGo}:${secondsToGo}`;
}

function parseSlResponse(response) {
  const sl = JSON.parse(response);
  const buses = sl.ResponseData.Buses;
  let body = '';

  buses.forEach((bus) => {
    const line = bus.LineNumber;
    const destination = bus.Destination;
    const departureTime = minutesUntilDeparture(bus.ExpectedDateTime);

    body += `Buss <b>${line}</b> mot <b>${destination}</b> avgår om <b id="${destination}"><span class="timer">${departureTime}</span></b><br />`;
  });

  return body;
}

function execute(res) {
  request(url, (error, response, body) => {
    console.log('statusCode:', response && response.statusCode);
    if (response.statusCode === 200) res.render('index', { buses: parseSlResponse(body) });
  });
}

app.get('/', (req, res) => {
  execute(res);
});

app.listen(3000, () => console.log('SL RealTime app listening on port 3000!'));
